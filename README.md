# Amazon Review Score Predictor

This NLP project is the result of the third project of the [Data Science](https://www.acamica.com/data-science)
career at Acámica.

The steps to fulfill the minimum requirements were:

* Perform a exploratory data analysis.
* State a hypothesis.
* Perform all necessary transformations to provide a tidy dataset.
* Train and evaluate a machine learning model and optimize its hyper-parameters.
* Interpret the results.
* Explain the difference between the expected and actual results.
* State next steps to improve the model.

Some complementary readings to improve the result are the following (spanish articles):

* [Deep learning, introducción práctica con Keras](https://torres.ai/deep-learning-inteligencia-artificial-keras/).
* [Predicciones con Incertidumbre](https://www.bbvaaifactory.com/es/improving-predictions-in-deep-learning-by-modelling-uncertainty-2/).
* [Aprendizaje automático teórico y avanzado con TensorFlow](https://www.tensorflow.org/resources/learn-ml/theoretical-and-advanced-machine-learning?hl=es-419).
